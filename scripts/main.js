/*********************
*   Robert Koteles
*   Web developer
*   2017
*********************/

/*********************
*  GENERAL FUNCTIONS
*********************/

var GeneralFunctions = {
    prevent_default: function (event) {
        'use strict';
        if (window.event) {
            window.event.returnValue = false;
        } else if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
    },
    backToTop: function (event) {
        'use strict';
        $('.back-to-top').on('click', function() {
            var scrollTo = 0;

            $('body,html').animate({
                scrollTop: scrollTo
            }, 500);

            return false;
        });
    }

};

/*********************
*  SPECIFIC FUNCTIONS
*********************/

var Header = {
    initialize: function () {
        var self = this;
        self.onScroll();
    },
    onScroll: function () { /*depends on scroll direction we use different class names*/
        'use strict';
        var self = this;
        var scrollDirection;
        var scrollPositionNew;
        var scrollPositionOld = $(window).scrollTop();
        $(window).on('scroll', function () {
            scrollPositionNew = $(window).scrollTop();
            if (scrollPositionOld < 140) {
                scrollDirection = '';
            } else {
                if (scrollPositionOld > scrollPositionNew) {
                    scrollDirection = 'scrolling-up';
                }
                if (scrollPositionOld < scrollPositionNew) {
                    scrollDirection = 'scrolling-down';
                }
            }
            scrollPositionOld = scrollPositionNew;
            self.handleState(scrollDirection);
        });
    },
    handleState: function (scrollDirection) {
        'use strict';
        $('body').removeClass('scrolling-up scrolling-down').addClass(scrollDirection);
    }
};

var Navigation = {
    initialize: function () {
        var self = this;
        $('#primary-menu li').mouseenter(function () {
            self.onOpen();
        });

        $('#primary-menu li').mouseleave(function () {
            self.onClose();
        });

        self.onClick();
    },
    onClick: function () { /*click events*/
        'use strict';
        var self = this;
        $('img').on('click', function() {
            window.location.href = "index.html";
        });
        $("#mobilemenu-trigger").on("click", function() {
            GeneralFunctions.prevent_default();
            $(this).parents("header").toggleClass("mobilemenu-open");
        });
        
        
    },
    onOpen: function () { /*if we have submenu*/
        'use strict';
        $('body').addClass('navigation-visible');
    },
    onClose: function () {
        'use strict';
        $('body').removeClass('navigation-visible');
    }
};

var Teaser = {
    initialize: function () {
        var self = this;
        var initSetEqualHeight = true;

        self.initClick();
        self.loadList();
    },
    setEqualHeight: function (_group) { /*set the height of teasers in the same row to equal*/
        var self = this;

        $(_group).addClass('visible');

        if (!self.initSetEqualHeight) {
            $(_group).removeClass('heightSet').css('height', 'auto');
        }

        self.initSetEqualHeight = false;

        if (parseInt($('.measurement').css('max-width'), 10) <= 480) {
            /* mobile */
            return false;
        }

        var _this;
        var _thisOffsetTop = 0;
        var _currentOffsetTop = 0;
        var prevMaxHeight = 0;
        var maxHeight = 0;
        var i = 0;

        $(_group).each(function () {

            _this = $(this);
            _thisOffsetTop = _this.offset().top;

            if (!i) {
                _currentOffsetTop = _thisOffsetTop;
                i += 1;
            }

            prevMaxHeight = maxHeight;
            maxHeight = (_this.outerHeight() > maxHeight) ? _this.outerHeight() : maxHeight;

            if (_currentOffsetTop < (_thisOffsetTop - 10)) {
                _currentOffsetTop = _thisOffsetTop;
                $('.heightSet').css('height', prevMaxHeight + 'px');
                maxHeight = _this.outerHeight();
                $('.heightSet').removeClass('heightSet');
            }
            _this.addClass('heightSet');
        });

        $('.heightSet').css('height', maxHeight + 'px');
        $('.heightSet').removeClass('heightSet');
    },
    initClick: function () {
        var self = this;

        $(".loadmore").on("click", function() {
            self.loadMore();        
        });
    },
    loadMore: function () {
        /*load next elements from JSON*/
        var self = this;
        self.loadList();
    },
    loadList: function () {
        var self = this;

        var start = $(".teasers").attr("data-start");
        var count = 20;
        var url = "./app/feed/ships.json";
        //var url = "https://p0toayyqla.execute-api.us-west-2.amazonaws.com/dev/ships";

        var $contentPane = $("main");
        var shipDataImagePath = "assets/images/teasers/";
        var shipDataImage = "container_ship.jpg";

        $.ajax( url, {
            dataType: 'json'

        }).done(function (data, textStatus, jqXHR) {

            if (data) {

                var total = data.ships.length;
                //var i = 0;
                var $list = $contentPane.find('.teasers');
                var i = 0;
                var j = 0;
                
                $.each(data.ships, function (key, obj) {
                    
                    i++;

                    if( i > start && j < count ) {
                        j++;
                        
                        //using RegExp for reading the origin from the owner property 
                        var countryOfOrigin = "unknown";
                        var matches = obj.owner.match(/\((.*?)\)/);
                        if (matches) {
                            countryOfOrigin = matches[1];
                        }

                        //building the html snippet
                        var html = '<div class="teaser popupData col-xs-12 col-sm-6 col-md-4 col-lg-3" data-id="' + obj.id + '" data-built="' + obj.built + '" data-name="' + obj.name + '" data-lengthmeters="' + obj.lengthMeters + '" data-beammeters="' + obj.beamMeters + '" data-maxteu="' + obj.maxTEU + '"  data-owner="' + obj.owner + '"  data-origin="' + countryOfOrigin + '"  data-grosstonnage="' + obj.grossTonnage + '" ><div class="teaser-inner">';

                        html += '<div class="teaser-img"><img src="'+ shipDataImagePath + shipDataImage +'" class="img-responsive" alt="Name of ship"></div>';

                        if (obj.name) {
                            html += '<h4 class="shipdata-name">' + obj.name + '</h4>';
                        }

                        html += '<p class="shipdata-origin"><span>Country of origin:</span>' + countryOfOrigin + '</p>';
            
                        if (obj.maxTEU) {
                            html += '<p class="shipdata-teu"><span>TEU:</span>' + obj.maxTEU + '</p>';
                        }

                        html += '</div></div>';

                        $list.append(html);

                        $(".teasers").attr("data-start", i);
                        
                        if ( i >= total ) {
                            $(".loadmore").remove();
                        }  
                    } 

                });

                self.setEqualHeight(".teaser-inner");
                self.initPopup(".popupData");
                $("#loading-msg").remove();
            }
        });
    },
    initPopup: function ( element ) {
        $( element ).on('click', function() {
            var $this = $(this);

            var html = '<h2>' + $this.data("name") + '</h2>';
                //html += '<p><span>ID:</span>' + $this.data("id") + '</p>';
                html += '<p><span>Built:</span>' + $this.data("built") + '</p>';                
                html += '<p><span>Length Meters:</span>' + $this.data("lengthmeters") + '</p>';
                html += '<p><span>Beam Meters:</span>' + $this.data("beammeters") + '</p>';
                html += '<p><span>Max TEU:</span>' + $this.data("maxteu") + '</p>';
                html += '<p><span>Owner:</span>' + $this.data("owner") + '</p>';
                html += '<p><span>Origin:</span>' + $this.data("origin") + '</p>';
                html += '<p><span>Gross Tonnage:</span>' + $this.data("grosstonnage") + '</p>';

                $('#modal-window .modal-body').html(html);
                $('#modal-window').modal('show', {backdrop: 'static'});

            return false;
        });

        
    }
};


var Footer = {
    initialize: function () {
        var self = this;
    }
};

$(function () {

    Header.initialize();
    Navigation.initialize();
    Teaser.initialize();
    Footer.initialize();

});


$(window).resize(function () {
     Teaser.setEqualHeight('.teaser-inner');
     $("main").addClass("sidebar-open");
});

$(window).on("load", function () {

});