# Cargo Mate - Interview task

This is the solution of the interview task for Cargo Mate.

![enter image description here](https://res-4.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_256,w_256,f_auto,q_auto:eco/v1505508357/pu4wa7gsvejwmnffn4k7.png)

**Last code update: 30/11/2017**

## Task description

Open the file from here:

    #task_description/CargoMate Interview Assignment 2017-11-23.pdf

## Demo

Click [here](http://domainforssl.hu/interviewtask/cargomate-task-bootstrap/) to see my solution.

---

@author : Robert Koteles
